import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SliderComponent } from './slider/slider.component';
import { RadioComponent } from './radio/radio.component';


const routes: Routes = [
  { path: '', component:SliderComponent},
  { path: 'radio', component:RadioComponent},
  { path: 'slider', component:SliderComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
